package pl.net.innotec.springangularlogin.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.net.innotec.springangularlogin.service.AuthenticationService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class BasicAuthController {

    @CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
    @GetMapping(path = "/basicauth")
    public AuthenticationService basicauth() {
        return new AuthenticationService("You are authenticated");
    }
}
